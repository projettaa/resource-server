## Description

- Serveur de ressource de notre application.

- API



## Installation

Install the dependencies by running the commands

```sh
$ npm install

$ bower install
```
## Usage

Pour lancer le projet, exécutez la commande suivante

```sh
$ mvn clean

$ mvn install
```

Puis lancer l'application ressource



## Déploiement

Pour déployer le projet, exécutez la commande

```sh
$ mvn package
```

Ensuite, déplacez le paquet war vers le répertoire webapps Tomcat



## License

Copyright (c) 2016 Projet TAA, ISTIC, Univ-Rennes 1


