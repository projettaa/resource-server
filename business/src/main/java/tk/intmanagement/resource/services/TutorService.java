package tk.intmanagement.resource.services;


import tk.intmanagement.resource.models.Tutor;

public interface TutorService {

    /**
     * Retrieve all tutors
     *
     * @return Iterable<Tutor>
     */
    Iterable<Tutor> findAll();

    /**
     * Get tutor by id
     *
     * @param id int
     * @return Tutor
     */
    Tutor findOne(int id);

    /**
     * Save a tutor in the database
     * Return the tutor if it was created, null otherwise
     *
     * @param tutor Tutor
     * @return Tutor
     */
    Tutor save(Tutor tutor);

    /**
     * Delete a tutor
     *
     * @param id int
     */
    void delete(int id);
}
