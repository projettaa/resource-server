package tk.intmanagement.resource.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.intmanagement.resource.dao.CompanyRepository;
import tk.intmanagement.resource.models.Company;

@Service("companyService")
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    CompanyRepository companyRepository;

    @Override
    public Iterable<Company> findAll() {
        return companyRepository.findAll();
    }

    @Override
    public Company findOne(int id) {
        return companyRepository.findOne(id);
    }

    @Override
    public Company save(Company company) {
        return companyRepository.save(company);
    }

    @Override
    public void delete(int id) {
        if (companyRepository.exists(id)) {
            companyRepository.delete(id);
        }
    }
}
