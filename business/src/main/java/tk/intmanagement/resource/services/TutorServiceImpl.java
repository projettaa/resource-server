package tk.intmanagement.resource.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.intmanagement.resource.dao.TutorRepository;
import tk.intmanagement.resource.models.Tutor;

@Service("tutorService")
public class TutorServiceImpl implements TutorService {
    @Autowired
    TutorRepository tutorRepository;

    @Override
    public Iterable<Tutor> findAll() {
        return tutorRepository.findAll();
    }

    @Override
    public Tutor findOne(int id) {
        return tutorRepository.findOne(id);
    }

    @Override
    public Tutor save(Tutor tutor) {
        return tutorRepository.save(tutor);
    }

    @Override
    public void delete(int id) {
        if (tutorRepository.exists(id)) {
            tutorRepository.delete(id);
        }
    }
}
