package tk.intmanagement.resource.services;


import tk.intmanagement.resource.models.Company;

public interface CompanyService {

    /**
     * Retrieve all companies
     *
     * @return Iterable<Company>
     */
    Iterable<Company> findAll();

    /**
     * Get company by id
     *
     * @param id int
     * @return Company
     */
    Company findOne(int id);

    /**
     * Save a company in the database
     * Return the company if it was created, null otherwise
     *
     * @param company Company
     * @return Company
     */
    Company save(Company company);

    /**
     * Delete a company
     *
     * @param id int
     */
    void delete(int id);
}

