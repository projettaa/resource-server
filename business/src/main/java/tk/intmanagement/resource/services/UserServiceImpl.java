package tk.intmanagement.resource.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import tk.intmanagement.resource.dao.UserRepository;
import tk.intmanagement.resource.models.Company;
import tk.intmanagement.resource.models.Internship;
import tk.intmanagement.resource.models.Tutor;
import tk.intmanagement.resource.models.User;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    protected UserRepository    userRepository;
    @Autowired
    private   InternshipService internshipService;
    @Autowired
    private   CompanyService    companyService;
    @Autowired
    private   TutorService      tutorService;

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findOne(int id) {
        return userRepository.findOne(id);
    }

    @Override
    public Set<User> findByIds(Set<User> users) {
        if (users == null) {
            return new HashSet<>();
        }
        return users.stream()
                    .map(user -> findOne(user.getId()))
                    .filter(user -> user != null)
                    .collect(Collectors.toSet());
    }

    @Override
    public boolean exists(User user) {
        return userRepository.exists(user.getId());
    }

    @Override
    public Internship addInternship(Internship internship) {
        internship.setNumber(UUID.randomUUID().toString());

        return addOrUpdateInternship(0, internship);
    }

    public Tutor addTutor(Tutor tutor) {
        return tutorService.save(tutor);
    }

    public Company addCompany(Company company) {
        return companyService.save(company);
    }

    @Override
    public Internship updateInternship(int id, Internship internship) {
        Internship savedInternship = internshipService.findOne(id);

        if (savedInternship != null) {
            // number does not change
            internship.setNumber(savedInternship.getNumber());

            return addOrUpdateInternship(id, internship);
        }

        return null;
    }

    @Override
    public Internship addOrUpdateInternship(int id, Internship internship) {
        if (id != 0) {
            internship.setId(id);
        }
        return internshipService.save(internship);
    }

    @Override
    public void deleteInternship(int id) {
        internshipService.delete(id);
    }

    @Override
    public Iterable<User> findByRoles(Set<String> roles) {
        return StreamSupport.stream(findAll().spliterator(), false)
                            .filter(user -> user.hasRoles(roles))
                            .collect(Collectors.toList());
    }

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User update(int id, User user) {
        User u = findOne(id);
        if (u != null) {
            // FIXME: not working
            // do not change username, password
            user.setUsername(u.getUsername());
            user.setPassword(u.getPassword());
            user.setRole(u.getRole());
            return save(user);
        } else {
            return null;
        }
    }

    @Override
    public void delete(int id) {
        userRepository.delete(id);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User getPrincipal(Principal principal) {
        User user = (User) ((OAuth2Authentication) principal).getPrincipal();
        // FIXME: 02/08/2016 I have to retrieve another time the user so that I can avoid the termination of the hibernate session
        // There may be another solution
        // Error : failed to lazily initialize a collection of role: ..., could not initialize proxy - no Session
        return findOne(user.getId());
    }
}
