package tk.intmanagement.resource.services;


import tk.intmanagement.resource.models.Teacher;

/**
 * Created by bouluad on 12/10/16.
 */
public interface TeacherService {


    /**
     * Retrieve all teachers
     *
     * @return Iterable<Teacher>
     */
    Iterable<Teacher> findAll();

    /**
     * Get teahcer by id
     *
     * @param id int
     * @return Teacher
     */
    Teacher findOne(int id);

    /**
     * Save a teacher in the database
     * Return the teacher if it was created, null otherwise
     *
     * @param teacher Teacher
     * @return teacher
     */
    Teacher save(Teacher teacher);

    /**
     * Delete a teacher
     *
     * @param id int
     */
    void delete(int id);


}
