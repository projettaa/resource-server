package tk.intmanagement.resource.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.intmanagement.resource.dao.RoleRepository;
import tk.intmanagement.resource.models.Role;

@Service("roleService")
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role save(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Role findOne(int id) {
        return roleRepository.findOne(id);
    }

    @Override
    public Iterable<Role> findAll() {
        return roleRepository.findAll();
    }

    @Override
    public Role findOrCreate(String authority) {
        Role byName = roleRepository.findByName(authority);
        if (byName == null) {
            byName = roleRepository.save(new Role(authority));
        }
        return byName;
    }
}
