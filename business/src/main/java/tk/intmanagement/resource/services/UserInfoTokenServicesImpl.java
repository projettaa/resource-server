package tk.intmanagement.resource.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import tk.intmanagement.resource.models.Student;
import tk.intmanagement.resource.models.User;

import java.util.LinkedHashMap;
import java.util.Map;

public class UserInfoTokenServicesImpl extends UserInfoTokenServices {

    @Autowired
    private UserService    userService;
    @Autowired
    private RoleService    roleService;
    @Autowired
    private StudentService studentService;

    public UserInfoTokenServicesImpl(String userInfoEndpointUrl, String clientId) {
        super(userInfoEndpointUrl, clientId);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Object getPrincipal(Map<String, Object> map) {
        String username  = (String) map.get("username");
        String role      = ((LinkedHashMap<String, String>) map.get("role")).get("name");
        String ine       = (String) map.get("ine");
        String birthDate = (String) map.get("birthDate");
        String sex       = (String) map.get("sex");
        String adress    = (String) map.get("adress");
        String mail      = (String) map.get("mail");
        String tel       = (String) map.get("tel");

        User user;

        if (role.equals("ROLE_USER")) {
            user = studentService.findByUsername(username);
        } else {
            user = userService.findByUsername(username);
        }

        boolean userChanged = false;

        if (user == null) {
            user = new User();
            user.setUsername(username);
            if (map.containsKey("firstName")) {
                user.setFirstName((String) map.get("firstName"));
            }

            if (map.containsKey("lastName")) {
                user.setLastName((String) map.get("lastName"));
            }

            userChanged = true;
        }

        if (map.containsKey("role")) {
            user.setRole(roleService.findOrCreate(role));
            userChanged = true;
        }

        if (userChanged) {
            if (user.is("ROLE_USER")) {
                user = studentService.save(new Student(user, ine, birthDate, sex, adress, mail, tel));
            } else {
                user = userService.save(user);
            }
        }

        return user;
    }
}