package tk.intmanagement.resource.services;


import tk.intmanagement.resource.models.Internship;

public interface InternshipService {

    /**
     * Retrieve all internships
     *
     * @return Iterable<Internship>
     */
    Iterable<Internship> findAll();

    /**
     * Get internship by id
     *
     * @param id int
     * @return Internship
     */
    Internship findOne(int id);

    /**
     * Save a internship in the database
     * Return the internship if it was created, null otherwise
     *
     * @param internship Internship
     * @return Internship
     */
    Internship save(Internship internship);

    /**
     * Delete a internship
     *
     * @param id int
     */
    void delete(int id);
}
