package tk.intmanagement.resource.services;


import tk.intmanagement.resource.models.Role;

public interface RoleService {

    /**
     * Save role in database
     * Return saved role object
     *
     * @param role Role
     * @return Role
     */
    Role save(Role role);

    /**
     * Find role by id
     *
     * @param id int
     * @return Role
     */
    Role findOne(int id);

    /**
     * Find all roles
     *
     * @return Iterable<Role>
     */
    Iterable<Role> findAll();

    /**
     * Find role or create it
     *
     * @param authority String
     * @return Role
     */
    Role findOrCreate(String authority);
}
