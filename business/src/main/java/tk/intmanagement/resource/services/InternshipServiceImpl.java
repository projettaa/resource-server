package tk.intmanagement.resource.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.intmanagement.resource.dao.InternshipRepository;
import tk.intmanagement.resource.models.Internship;

@Service("internshipService")
public class InternshipServiceImpl implements InternshipService {
    @Autowired
    InternshipRepository internshipRepository;

    @Override
    public Iterable<Internship> findAll() {
        return internshipRepository.findAll();
    }

    @Override
    public Internship findOne(int id) {
        return internshipRepository.findOne(id);
    }

    @Override
    public Internship save(Internship internship) {
        return internshipRepository.save(internship);
    }

    @Override
    public void delete(int id) {
        if (internshipRepository.exists(id)) {
            internshipRepository.delete(id);
        }
    }
}
