package tk.intmanagement.resource.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.intmanagement.resource.dao.StudentRepository;
import tk.intmanagement.resource.models.Student;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service("etudiantService")
public class StudentServiceImpl implements StudentService {
    @Autowired
    protected StudentRepository studentRepository;

    @Override
    public Student save(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student findByUsername(String username) {
        return studentRepository.findByUsername(username);
    }

    @Override
    public Student findOne(int id) {
        return studentRepository.findOne(id);
    }

    @Override
    public Set<Student> findByIds(Set<Student> students) {

        if (students == null) {
            return new HashSet<>();
        }
        return students.stream()
                       .map(student -> findOne(student.getId()))
                       .filter(student -> student != null)
                       .collect(Collectors.toSet());
    }

    @Override
    public boolean exists(Student student) {
        return studentRepository.exists(student.getId());
    }

    @Override
    public Iterable<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student update(Student student) {
        Student s = findOne(student.getId());
        if (s != null) {

            student.setINE(s.getINE());
            student.setFirstName(s.getFirstName());
            student.setLastName(s.getLastName());
            student.setBirthDate(s.getBirthDate());
            student.setSex(s.getSex());
            student.setAddress(s.getAddress());
            student.setMail(s.getMail());
            student.setTel(s.getTel());
            student.setUsername(s.getUsername());
            student.setPassword(s.getPassword());

            // TODO :  add all attributs of students

            return save(student);

        } else {
            return null;
        }
    }

    @Override
    public void delete(int id) {

        studentRepository.delete(id);
    }

    @Override
    public Student getPrincipal(Principal principal) {
        return null;
    }
}
