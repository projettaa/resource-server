package tk.intmanagement.resource.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.intmanagement.resource.dao.TeacherRepository;
import tk.intmanagement.resource.models.Teacher;

/**
 * Created by bouluad on 25/10/16.
 */

@Service("teacherService")
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    TeacherRepository teacherRepository;

    @Override
    public Iterable<Teacher> findAll() {
        return teacherRepository.findAll();
    }

    @Override
    public Teacher findOne(int id) {
        return teacherRepository.findOne(id);
    }

    @Override
    public Teacher save(Teacher teacher) {
        return teacherRepository.save(teacher);
    }

    @Override
    public void delete(int id) {

        if (teacherRepository.exists(id)) {
            teacherRepository.delete(id);
        }

    }
}
