package tk.intmanagement.resource.services;


import tk.intmanagement.resource.models.Student;

import java.security.Principal;
import java.util.Set;

public interface StudentService {
    /**
     * Save user in database
     * Return saved user object
     *
     * @param student Etudiant
     * @return Etudiant
     */
    Student save(Student student);


    /**
     * Find student by username
     *
     * @param username String
     * @return Stident
     */
    Student findByUsername(String username);


    /**
     * Find student by id
     *
     * @param id int
     * @return Student
     */
    Student findOne(int id);

    /**
     * Find all students using the ids passed in the set students
     *
     * @param students Set<Student>
     * @return Set<User>
     */
    Set<Student> findByIds(Set<Student> students);

    /**
     * Checks whether a student exists or not
     * Return true if s/he exists, false otherwise
     *
     * @param student Student
     * @return boolean
     */
    boolean exists(Student student);


    /**
     * Find all students
     *
     * @return Iterable<Student>
     */
    Iterable<Student> findAll();

    /**
     * Update student
     * Return updated student object
     *
     * @param student Student
     * @param student Student
     * @return User
     */
    Student update(Student student);

    /**
     * Delete student
     *
     * @param id int
     */
    void delete(int id);


    /**
     * Get student from spring security principal
     *
     * @param principal {@link Principal}
     * @return {@link Student}
     */
    Student getPrincipal(Principal principal);


}
