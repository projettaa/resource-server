package tk.intmanagement.resource.services;

import tk.intmanagement.resource.models.Company;
import tk.intmanagement.resource.models.Internship;
import tk.intmanagement.resource.models.Tutor;
import tk.intmanagement.resource.models.User;

import java.security.Principal;
import java.util.Set;

public interface UserService {

    /**
     * Save user in database
     * Return saved user object
     *
     * @param user User
     * @return User
     */
    User save(User user);

    /**
     * Find user by id
     *
     * @param id int
     * @return User
     */
    User findOne(int id);

    /**
     * Find all users using the ids passed in the set users
     *
     * @param users Set<User>
     * @return Set<User>
     */
    Set<User> findByIds(Set<User> users);

    /**
     * Checks whether a user exists or not
     * Return true if s/he exists, false otherwise
     *
     * @param user User
     * @return boolean
     */
    boolean exists(User user);

    /**
     * Add an internship
     * Return the internship if it was added, null otherwise
     *
     * @param internship Internship
     * @return Internship
     */
    Internship addInternship(Internship internship);

    /**
     * Update an internship
     * Return the internship if it was updated, null otherwise
     *
     * @param id         int
     * @param internship Internship
     * @return Internship
     */
    Internship updateInternship(int id, Internship internship);

    /**
     * Add or Update an internship
     * Return the internship if it was created/updated, null otherwise
     * Note that for a creation id must be 0
     *
     * @param id         int
     * @param internship Internship
     * @return Internship
     */
    Internship addOrUpdateInternship(int id, Internship internship);

    /**
     * Delete a internship
     *
     * @param id int
     */
    void deleteInternship(int id);

    /**
     * Add an tutor
     * Return the tutor if it was added, null otherwise
     *
     * @param tutor Tutor
     * @return Tutor
     */
    Tutor addTutor(Tutor tutor);

    /**
     * Add an company
     * Return the company if it was added, null otherwise
     *
     * @param company Company
     * @return Company
     */
    Company addCompany(Company company);

    /**
     * Find users by roles slugs
     *
     * @param roles Set<String>
     * @return Iterable<User>
     */
    Iterable<User> findByRoles(Set<String> roles);

    /**
     * Find all users
     *
     * @return Iterable<User>
     */
    Iterable<User> findAll();

    /**
     * Update user
     * Return updated user object
     *
     * @param id   int
     * @param user User
     * @return User
     */
    User update(int id, User user);

    /**
     * Delete user
     *
     * @param id int
     */
    void delete(int id);

    /**
     * Find user by username
     *
     * @param username String
     * @return User
     */
    User findByUsername(String username);

    /**
     * Get user from spring security principal
     *
     * @param principal {@link Principal}
     * @return {@link User}
     */
    User getPrincipal(Principal principal);
}
