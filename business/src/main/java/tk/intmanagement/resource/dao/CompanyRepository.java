package tk.intmanagement.resource.dao;

import org.springframework.stereotype.Repository;
import tk.intmanagement.resource.models.Company;

@Repository
public interface CompanyRepository extends BaseRepository<Company> {
}
