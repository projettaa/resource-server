package tk.intmanagement.resource.dao;

import org.springframework.stereotype.Repository;
import tk.intmanagement.resource.models.Student;

@Repository
public interface StudentRepository extends BaseRepository<Student> {
    Student findByUsername(String username);
}
