package tk.intmanagement.resource.dao;


import tk.intmanagement.resource.models.Teacher;

/**
 * Created by bouluad on 12/10/16.
 */
public interface TeacherRepository extends BaseRepository<Teacher>  {
}
