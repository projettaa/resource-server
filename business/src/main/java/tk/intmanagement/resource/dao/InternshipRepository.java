package tk.intmanagement.resource.dao;

import org.springframework.stereotype.Repository;
import tk.intmanagement.resource.models.Internship;

@Repository
public interface InternshipRepository extends BaseRepository<Internship> {
}
