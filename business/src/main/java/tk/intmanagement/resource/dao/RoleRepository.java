package tk.intmanagement.resource.dao;

import org.springframework.stereotype.Repository;
import tk.intmanagement.resource.models.Role;

@Repository
public interface RoleRepository extends BaseRepository<Role> {
    Role findByName(String name);
}