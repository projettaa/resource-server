package tk.intmanagement.resource.dao;

import org.springframework.stereotype.Repository;
import tk.intmanagement.resource.models.User;

@Repository
public interface UserRepository extends BaseRepository<User> {
    User findByUsername(String username);
}