package tk.intmanagement.resource.dao;

import org.springframework.stereotype.Repository;
import tk.intmanagement.resource.models.Tutor;

@Repository
public interface TutorRepository extends BaseRepository<Tutor> {
}
