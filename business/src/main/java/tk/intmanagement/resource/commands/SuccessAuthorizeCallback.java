package tk.intmanagement.resource.commands;

import org.springframework.http.ResponseEntity;
import tk.intmanagement.resource.models.Internship;
import tk.intmanagement.resource.models.User;

public interface SuccessAuthorizeCallback {
    ResponseEntity<Internship> run(User user);
}
