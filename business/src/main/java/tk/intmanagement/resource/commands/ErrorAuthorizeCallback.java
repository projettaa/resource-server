package tk.intmanagement.resource.commands;

import org.springframework.http.ResponseEntity;
import tk.intmanagement.resource.models.Internship;

public interface ErrorAuthorizeCallback {
    ResponseEntity<Internship> run();
}
