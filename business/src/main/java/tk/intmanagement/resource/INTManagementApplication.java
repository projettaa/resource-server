package tk.intmanagement.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class INTManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(INTManagementApplication.class, args);
    }
}