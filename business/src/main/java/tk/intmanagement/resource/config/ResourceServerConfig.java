package tk.intmanagement.resource.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import tk.intmanagement.resource.services.UserInfoTokenServicesImpl;

@Configuration
@EnableResourceServer
public class ResourceServerConfig {
    @Autowired
    private ResourceServerProperties sso;

    @Bean
    public ResourceServerTokenServices myUserInfoTokenServices() {
        return new UserInfoTokenServicesImpl(sso.getUserInfoUri(), sso.getClientId());
    }
}
