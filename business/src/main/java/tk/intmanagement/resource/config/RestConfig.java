package tk.intmanagement.resource.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class RestConfig {

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate                  restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> converters   = restTemplate.getMessageConverters();
        converters.stream().filter(converter -> converter instanceof MappingJackson2HttpMessageConverter).forEach(converter -> {
            MappingJackson2HttpMessageConverter jsonConverter = (MappingJackson2HttpMessageConverter) converter;
            jsonConverter.setObjectMapper(new ObjectMapper());
            List<MediaType> list = new ArrayList<>();
            list.add(new MediaType("*", "json", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET));
            list.add(new MediaType("*", "octet-stream", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET));
            list.add(new MediaType("text", "javascript", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET));
            jsonConverter.setSupportedMediaTypes(list);
        });
        return restTemplate;
    }
}
