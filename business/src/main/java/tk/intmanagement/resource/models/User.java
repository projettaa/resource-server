package tk.intmanagement.resource.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.voodoodyne.jackson.jsog.JSOGGenerator;
import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.InheritanceType.JOINED;

@Entity
@Table(name = "users")
@Inheritance(strategy = JOINED)
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class User implements Serializable {
    @Id
    @GeneratedValue
    protected int    id;
    @Column(unique = true)
    protected String username;
    @JsonIgnore
    protected String password;
    protected String firstName;
    protected String lastName;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    protected Role   role;

    public User() {
        password = "";
    }

    public User(int id, String firstName, String lastName, String username, String password, Role role) {
        this();
        setId(id);
        setFirstName(firstName);
        setLastName(lastName);
        setUsername(username);
        setPassword(password);
        setRole(role);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty(value = "password")
    public void setPassword(String password) {
        this.password = BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    // TODO: 08/07/2016 Add documentation
    public boolean hasRoles(Set<String> roles) {
        return roles.stream()
                    .allMatch(role -> role.equals(getRole().getName()));
    }

    public boolean is(String role) {
        return hasRoles(new HashSet<String>() {{
            add(role);
        }});
    }
}
