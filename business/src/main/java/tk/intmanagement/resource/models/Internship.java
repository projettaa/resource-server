package tk.intmanagement.resource.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by bouluad on 12/10/16.
 */

@Entity
@Table(name = "internships")
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Internship implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull
    @Column(name = "number", nullable = false)
    private String number;

    @Column(name = "mission")
    private String mission;

    @Column(name = "address")
    private String address;

    @Column(name = "gratification")
    private int gratification;

    @Column(name = "startDate")
    private Date startDate;

    @Column(name = "endDate")
    private Date endDate;

    @Column(name = "numberOfDays")
    private int numberOfDays;

    @Column(name = "numberOfWeeks")
    private int numberOfWeeks;

    @Column(name = "levelOfStudy")
    private String levelOfStudy;

    @ManyToOne
    private Student student;

    @ManyToOne
    private Teacher teacher;

    @ManyToOne(cascade = CascadeType.ALL)
    private Tutor tutor;

    @ManyToOne(cascade = CascadeType.ALL)
    private Company company;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getGratification() {
        return gratification;
    }

    public void setGratification(int gratification) {
        this.gratification = gratification;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public int getNumberOfWeeks() {
        return numberOfWeeks;
    }

    public void setNumberOfWeeks(int numberOfWeeks) {
        this.numberOfWeeks = numberOfWeeks;
    }

    public String getLevelOfStudy() {
        return levelOfStudy;
    }

    public void setLevelOfStudy(String levelOfStudy) {
        this.levelOfStudy = levelOfStudy;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "Internship{" +
               "id=" + id +
               ", number='" + number + '\'' +
               ", mission='" + mission + '\'' +
               ", address='" + address + '\'' +
               ", gratification=" + gratification +
               ", startDate=" + startDate +
               ", endDate=" + endDate +
               ", numberOfDays=" + numberOfDays +
               ", numberOfWeeks=" + numberOfWeeks +
               ", levelOfStudy='" + levelOfStudy + '\'' +
               ", student=" + student +
               ", teacher=" + teacher +
               ", tutor=" + tutor +
               ", company=" + company +
               '}';
    }
}
