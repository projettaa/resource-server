package tk.intmanagement.resource.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tk.intmanagement.resource.models.Company;
import tk.intmanagement.resource.services.CompanyService;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    private CompanyService companyService;


    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasAuthority(('{authority=ROLE_ADMIN}'))")
    public Iterable<Company> index() {
        return companyService.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasAuthority(('{authority=ROLE_ADMIN}'))")
    public ResponseEntity<Company> store(@RequestBody Company company) {
        company = companyService.save(company);
        return company == null ? null : new ResponseEntity<>(company, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority(('{authority=ROLE_ADMIN}'))")
    public void delete(@PathVariable int id) {
        companyService.delete(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority(('{authority=ROLE_ADMIN}'))")
    public ResponseEntity<Company> addOrUpdate(@RequestBody Company company, @PathVariable int id) {
        company = companyService.save(company);
        return company == null ? null : new ResponseEntity<>(company, HttpStatus.OK);
    }

}
