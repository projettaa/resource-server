package tk.intmanagement.resource.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tk.intmanagement.resource.models.Internship;
import tk.intmanagement.resource.models.Student;
import tk.intmanagement.resource.services.InternshipService;

import java.security.Principal;

@RestController
@RequestMapping("/internships")
public class InternshipController extends Controller {
    @Autowired
    private InternshipService internshipService;

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasAuthority(('{authority=ROLE_ADMIN}'))")
    public Iterable<Internship> index() {
        return internshipService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('{authority=ROLE_USER}', '{authority=ROLE_ADMIN}')")
    public ResponseEntity<Internship> findOne(Principal principal, @PathVariable int id) {
        Internship internship = internshipService.findOne(id);

        return authorizeStudentOrAdmin(principal, internship, (user) -> new ResponseEntity<>(internship, HttpStatus.OK), () -> new ResponseEntity<>(HttpStatus.FORBIDDEN));
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public ResponseEntity<Internship> store(Principal principal, @RequestBody Internship internship) {
        Student student = (Student) userService.getPrincipal(principal);
        internship.setStudent(student);

        internship = userService.addInternship(internship);

        return internship == null ? null : new ResponseEntity<>(internship, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public ResponseEntity<Internship> delete(Principal principal, @PathVariable int id) {
        Internship internship = internshipService.findOne(id);

        return authorizeStudent(principal, internship, (user) -> {
            userService.deleteInternship(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }, () -> new ResponseEntity<>(HttpStatus.FORBIDDEN));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("hasAnyAuthority('{authority=ROLE_USER}', '{authority=ROLE_ADMIN}')")
    public ResponseEntity<Internship> update(Principal principal, @RequestBody Internship internship, @PathVariable int id) {
        Internship savedInternship = internshipService.findOne(id);

        return authorizeStudentOrAdmin(principal, savedInternship, (user) -> {
            Internship internshipToSave;
            if (user.is("ROLE_ADMIN")) {
                // only modify teacher
                internshipToSave = savedInternship;
                internshipToSave.setTeacher(internship.getTeacher());
            } else {
                internshipToSave = internship;
            }

            internshipToSave = userService.updateInternship(id, internshipToSave);
            return new ResponseEntity<>(internshipToSave, HttpStatus.OK);
        }, () -> new ResponseEntity<>(HttpStatus.FORBIDDEN));
    }
}