package tk.intmanagement.resource.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tk.intmanagement.resource.models.Teacher;
import tk.intmanagement.resource.services.TeacherService;

@RestController
@RequestMapping("/teachers")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;


    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasAuthority(('{authority=ROLE_ADMIN}'))")
    public Iterable<Teacher> index() {
        return teacherService.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasAuthority(('{authority=ROLE_ADMIN}'))")
    public ResponseEntity<Teacher> store(@RequestBody Teacher teacher) {
        teacher = teacherService.save(teacher);
        return teacher == null ? null : new ResponseEntity<>(teacher, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority(('{authority=ROLE_ADMIN}'))")
    public void delete(@PathVariable int id) {
        teacherService.delete(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority(('{authority=ROLE_ADMIN}'))")
    public ResponseEntity<Teacher> addOrUpdate(@RequestBody Teacher teacher, @PathVariable int id) {
        teacher = teacherService.save(teacher);
        return teacher == null ? null : new ResponseEntity<>(teacher, HttpStatus.OK);
    }

}
