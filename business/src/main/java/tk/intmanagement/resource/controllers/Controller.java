package tk.intmanagement.resource.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import tk.intmanagement.resource.commands.ErrorAuthorizeCallback;
import tk.intmanagement.resource.commands.SuccessAuthorizeCallback;
import tk.intmanagement.resource.models.Internship;
import tk.intmanagement.resource.models.Student;
import tk.intmanagement.resource.models.User;
import tk.intmanagement.resource.services.UserService;

import java.security.Principal;

public class Controller {

    @Autowired
    protected UserService userService;

    protected ResponseEntity<Internship> authorizeStudent(Principal principal, Internship internship, SuccessAuthorizeCallback success, ErrorAuthorizeCallback error) {
        Student student = (Student) userService.getPrincipal(principal);

        if (internship != null && student.getId() == internship.getStudent().getId()) {
            return success.run(student);
        } else {
            return error.run();
        }
    }

    protected ResponseEntity<Internship> authorizeStudentOrAdmin(Principal principal, Internship internship, SuccessAuthorizeCallback success, ErrorAuthorizeCallback error) {
        User user = userService.getPrincipal(principal);

        if (user.is("ROLE_ADMIN")) {
            return success.run(user);
        }

        return authorizeStudent(principal, internship, success, error);
    }
}
