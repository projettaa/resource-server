package tk.intmanagement.resource.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tk.intmanagement.resource.models.Student;
import tk.intmanagement.resource.models.User;
import tk.intmanagement.resource.services.StudentService;

import java.security.Principal;
import java.util.Set;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentService studentService;


    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasAuthority(('{authority=ROLE_ADMIN}'))")
    public Iterable<Student> index(@RequestParam(required = false) Set<String> roles) {
        return studentService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority(('{authority=ROLE_ADMIN}'))")
    public void delete(@PathVariable int id) {
        studentService.delete(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> store(@RequestBody Student student) {
        student = studentService.save(student);
        return student == null ? null : new ResponseEntity<>(student, HttpStatus.CREATED);
    }

    /**
     * Return the connected student
     *
     * @return Student
     */
    @RequestMapping(value = "/me", method = RequestMethod.GET)
    public User me(Principal principal) {
        return studentService.getPrincipal(principal);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority(('{authority=ROLE_ADMIN}'))")
    public ResponseEntity<Student> update(@RequestBody Student student) {
        student = studentService.update(student);
        return student == null ? null : new ResponseEntity<>(student, HttpStatus.OK);
    }

}
