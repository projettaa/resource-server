package tk.intmanagement.resource.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tk.intmanagement.resource.models.Internship;
import tk.intmanagement.resource.models.Student;
import tk.intmanagement.resource.models.User;
import tk.intmanagement.resource.services.InternshipService;

import java.security.Principal;
import java.util.Set;

@RestController
@RequestMapping("/users")
public class UserController extends Controller {
    @Autowired
    private InternshipService internshipService;

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<User> index(@RequestParam(required = false) Set<String> roles) {
        if (roles == null || roles.isEmpty()) {
            return userService.findAll();
        } else {
            return userService.findByRoles(roles);
        }
    }

    @RequestMapping(value = "/{id}/internships", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public ResponseEntity<Iterable<Internship>> findAllInternships(Principal principal, @PathVariable int id) {
        Student student = (Student) userService.getPrincipal(principal);

        if (student.getId() == id) {
            return new ResponseEntity<>(student.getInternships(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        userService.delete(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> store(@RequestBody User user) {
        user = userService.save(user);
        return user == null ? null : new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    /**
     * Return the connected user
     *
     * @return User
     */
    @RequestMapping(value = "/me", method = RequestMethod.GET)
    public User me(Principal principal) {
        return userService.getPrincipal(principal);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> update(@PathVariable int id, @RequestBody User user) {
        user = userService.update(id, user);
        return user == null ? null : new ResponseEntity<>(user, HttpStatus.OK);
    }
}